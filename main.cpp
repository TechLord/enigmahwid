#define _WIN32_WINNT 0x0500
#include <windows.h>
#include <stdio.h>
#include "hwbp.h"

//hwid variables
static ULONG_PTR hwidAddr = 0x4F16C; //hwid patch rva
static const char* newHwid = "0000DEADCAFEBABEF00D";

//global variables
#define DBG_PRINTEXCEPTION_C 0x40010006
#define TRAP_FLAG 0x100
static char dprintf_msg[66000];
static bool bRestoreHardwareBreakpoint = false;
static ULONG_PTR dr7backup = 0;

static void dprintf(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    *dprintf_msg=0;
    vsnprintf(dprintf_msg, sizeof(dprintf_msg), format, args);
    OutputDebugStringA(dprintf_msg);
}

static void dputs(const char* text)
{
    dprintf("%s\n", text);
}

static void changeHWID(char** hwid)
{
    dprintf("[HWID] Old HWID: \"%s\"", *hwid);
    strcpy(*hwid, newHwid);
    dprintf("[HWID] New HWID: \"%s\"", *hwid);
}

static LONG CALLBACK VectoredHandler(PEXCEPTION_POINTERS ExceptionInfo)
{
    DWORD ExceptionCode = ExceptionInfo->ExceptionRecord->ExceptionCode;
    if(ExceptionCode == DBG_PRINTEXCEPTION_C)
        return EXCEPTION_CONTINUE_SEARCH;
    if(ExceptionCode == EXCEPTION_SINGLE_STEP)
    {
        if(bRestoreHardwareBreakpoint)
        {
            bRestoreHardwareBreakpoint = false;
            ExceptionInfo->ContextRecord->Dr7 = dr7backup;
            dputs("[HWID] HWBP restored!");
            return EXCEPTION_CONTINUE_EXECUTION;
        }
        else if((ULONG_PTR)ExceptionInfo->ExceptionRecord->ExceptionAddress==hwidAddr)
        {
            static int c=0;
            dprintf("[HWID] %d\n", c++);
            dputs("[HWID] HWBP hit (HWID)!");
            dr7backup = ExceptionInfo->ContextRecord->Dr7;
            ExceptionInfo->ContextRecord->Dr7 = 0;
            bRestoreHardwareBreakpoint = true;
            ExceptionInfo->ContextRecord->EFlags |= TRAP_FLAG;
            changeHWID((char**)ExceptionInfo->ContextRecord->Edi);
            return EXCEPTION_CONTINUE_EXECUTION;
        }
    }
    else
        dprintf("[HWID] Exception 0x%.8X", ExceptionCode);
    return EXCEPTION_CONTINUE_SEARCH;
}

extern "C" __declspec(dllexport) BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    static bool bHooked=false;
    if(!bHooked)
    {
        bHooked=true;
        AddVectoredExceptionHandler(1, VectoredHandler);
        hwidAddr+=(ULONG_PTR)GetModuleHandleA(0); //add imagebase of main executable
        if(hwbpSet(GetCurrentThread(), hwidAddr, 0, TYPE_EXECUTE, SIZE_1))
            dprintf("[HWID] HWBP set on 0x%.8X (HWID)!", hwidAddr);
        else
            dputs("[HWID] Failed to set HWBP (HWID)!");
    }
    return TRUE; // successful
}
